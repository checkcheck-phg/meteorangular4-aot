import { MongoObservable } from "meteor-rxjs";
import { Meteor } from "meteor/meteor";

import { ReferenceData } from "./../index";

export const ReferenceDataCollection = new MongoObservable.Collection<ReferenceData>("ReferenceData");