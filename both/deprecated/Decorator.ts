import "reflect-metadata";
import { Class } from "@angular/core";

/**
 * @deprecated
 * TODO[HCN] Angular 4 does not export yet
 */
export class Decorator {
    /**
     * @suppress {globalThis}
     * @param {?} name
     * @param {?} props
     * @param {?=} parentClass
     * @param {?=} chainFn
     * @return {?}
     */
    static makeDecorator(name, props, parentClass?, chainFn?): any {
        const /** @type {?} */ metaCtor = Decorator.makeMetadataCtor([props]);
        /**
         * @param {?} objOrType
         * @return {?}
         */
        function DecoratorFactory(objOrType): any {
            if (!(Reflect && Reflect.getOwnMetadata)) {
                throw "reflect-metadata shim is required when using class decorators";
            }
            if (this instanceof DecoratorFactory) {
                metaCtor.call(this, objOrType);
                return this;
            }
            const /** @type {?} */ annotationInstance = new (<any>((DecoratorFactory)))(objOrType);
            const /** @type {?} */ chainAnnotation = typeof this === "function" && Array.isArray(this.annotations) ? this.annotations : [];
            chainAnnotation.push(annotationInstance);
            const /** @type {?} */ TypeDecorator = (function TypeDecorator(cls) {
                const /** @type {?} */ annotations = Reflect.getOwnMetadata("annotations", cls) || [];
                annotations.push(annotationInstance);
                Reflect.defineMetadata("annotations", annotations, cls);
                return cls;
            });
            (<any>TypeDecorator).annotations = chainAnnotation;
            (<any>TypeDecorator).Class = Class;
            if (chainFn) chainFn(TypeDecorator);
            return TypeDecorator;
        }
        if (parentClass) {
            DecoratorFactory.prototype = Object.create(parentClass.prototype);
        }
        DecoratorFactory.prototype.toString = () => `@${name}`;
        (<any>((DecoratorFactory))).annotationCls = DecoratorFactory;
        return DecoratorFactory;
    }

    /**
     * @param {?} props
     * @return {?}
     */
    static makeMetadataCtor(props): any {
        return function ctor(...args) {
            props.forEach((prop, i) => {
                const /** @type {?} */ argVal = args[i];
                if (Array.isArray(prop)) {
                    // plain parameter
                    this[prop[0]] = argVal === undefined ? prop[1] : argVal;
                }
                else {
                    for (const /** @type {?} */ propName in prop) {
                        this[propName] =
                            argVal && argVal.hasOwnProperty(propName) ? argVal[propName] : prop[propName];
                    }
                }
            });
        };
    }

    /**
     * @param {?} name
     * @param {?} props
     * @param {?=} parentClass
     * @return {?}
     */
    static makeParamDecorator(name, props, parentClass?) {
        const /** @type {?} */ metaCtor = Decorator.makeMetadataCtor(props);
        /**
         * @param {...?} args
         * @return {?}
         */
        function ParamDecoratorFactory(...args) {
            if (this instanceof ParamDecoratorFactory) {
                metaCtor.apply(this, args);
                return this;
            }
            const /** @type {?} */ annotationInstance = new (<any>((ParamDecoratorFactory)))(...args);
            (<any>((ParamDecorator))).annotation = annotationInstance;
            return ParamDecorator;
            /**
             * @param {?} cls
             * @param {?} unusedKey
             * @param {?} index
             * @return {?}
             */
            function ParamDecorator(cls, unusedKey, index) {
                const /** @type {?} */ parameters = Reflect.getOwnMetadata("parameters", cls) || [];
                // there might be gaps if some in between parameters do not have annotations.
                // we pad with nulls.
                while (parameters.length <= index) {
                    parameters.push(null);
                }
                parameters[index] = parameters[index] || []; /** @type {?} */
                ((parameters[index])).push(annotationInstance);
                Reflect.defineMetadata("parameters", parameters, cls);
                return cls;
            }
        }
        if (parentClass) {
            ParamDecoratorFactory.prototype = Object.create(parentClass.prototype);
        }
        ParamDecoratorFactory.prototype.toString = () => `@${name}`;
        (<any>((ParamDecoratorFactory))).annotationCls = ParamDecoratorFactory;
        return ParamDecoratorFactory;
    }

    /**
     * @param {?} name
     * @param {?} props
     * @param {?=} parentClass
     * @return {?}
     */
    static makePropDecorator(name, props, parentClass?) {
        const /** @type {?} */ metaCtor = Decorator.makeMetadataCtor(props);
        /**
         * @param {...?} args
         * @return {?}
         */
        function PropDecoratorFactory(...args) {
            if (this instanceof PropDecoratorFactory) {
                metaCtor.apply(this, args);
                return this;
            }
            const /** @type {?} */ decoratorInstance = new (<any>((PropDecoratorFactory)))(...args);
            return function PropDecorator(target, name) {
                const /** @type {?} */ meta = Reflect.getOwnMetadata("propMetadata", target.constructor) || {};
                meta[name] = meta.hasOwnProperty(name) && meta[name] || [];
                meta[name].unshift(decoratorInstance);
                Reflect.defineMetadata("propMetadata", meta, target.constructor);
            };
        }
        if (parentClass) {
            PropDecoratorFactory.prototype = Object.create(parentClass.prototype);
        }
        PropDecoratorFactory.prototype.toString = () => `@${name}`;
        (<any>((PropDecoratorFactory))).annotationCls = PropDecoratorFactory;
        return PropDecoratorFactory;
    }
}