export enum ELogType {
    Info,
    Debug,
    Warn,
    Error,
    Trace
}