import { ELogType } from "../enums/ELogType";

export interface ILogInfo {
    Message: string;
    Type: ELogType;
    user: string;
}