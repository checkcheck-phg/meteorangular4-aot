import { BaseModel } from "./BaseModel";
import { ReferenceDataValue } from "./ReferenceDataValue";

export class ReferenceData extends BaseModel {
    Type: string;
    ReferenceDataValues: ReferenceDataValue[];
}