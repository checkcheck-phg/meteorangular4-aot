import { BaseModel } from "./BaseModel";

export class ReferenceDataValue {
    Code: string;
    Text: string;
    Translation?: string;
    Properties?: Object;
}