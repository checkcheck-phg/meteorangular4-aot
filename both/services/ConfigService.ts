import { Meteor } from "meteor/meteor";
import { Injectable } from "@angular/core";

interface ITwilioSettings {
    number: string;
    sid: string;
    authToken: string;
}

interface IExternals {
    twilio?: ITwilioSettings;
    services: any[];
}

interface IEmailSettings {
    from: string;
}

export interface IPublicSettings {}

export interface ISettings {
    production?: boolean;
    email?: IEmailSettings;
    externals?: IExternals;
    public: IPublicSettings;
}

declare var Package;

@Injectable()
export class ConfigService {
    private serviceConfiguration;

    get settings(): ISettings | IPublicSettings {
        return Meteor.isServer
            ? Meteor.settings as ISettings
            : Meteor.settings.public as IPublicSettings;
    }

    get isProduction(): boolean {
        return Boolean((<ISettings>this.settings).production);
    }
}