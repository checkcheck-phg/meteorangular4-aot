import { Injectable } from "@angular/core";

import { ConfigService } from "./ConfigService";
import { ILogInfo } from "../interfaces/ILogInfo";
import { ELogType } from "../enums/ELogType";

@Injectable()
export class LogService {
    constructor(private configService: ConfigService) {

    }
    debug(msg: string): void {
        this.log(msg, ELogType.Debug);
    }

    info(msg: string): void {
        this.log(msg, ELogType.Info);
    }

    warn(msg: string): void {
        this.log(msg, ELogType.Warn);
    }

    error(msg: string): void {
        this.log(msg, ELogType.Error);
    }

    trace(msg: string): void {
        this.log(msg, ELogType.Trace);
    }

    private log(msg: string, type: ELogType): void {
        if (this.configService.isProduction) {
            this.saveLog(msg, type);
            return;
        }

        switch (type) {
            case ELogType.Debug:
            case ELogType.Info:
            case ELogType.Trace:
            case ELogType.Warn:
                console[ELogType[type].toLowerCase()](msg);
                break;
            case ELogType.Error:
                console.error(msg);
                break;
        }
    }

    private saveLog(msg: string, type: ELogType): void {
         switch (type) {
            case ELogType.Info:
            case ELogType.Trace:
            case ELogType.Error:
                //TODO Using ILogInfo to save to log file
                break;
        }
    }
}




