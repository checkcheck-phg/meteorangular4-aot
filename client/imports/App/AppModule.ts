import { NgModule, ErrorHandler } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { AgmCoreModule } from "angular2-google-maps/core";

import { InfrastructureModule, ExceptionHandler, INFRASTRUCTURE_INJECTABLES } from "../Infrastructure";
import { APP_COMPONENTS, APP_ENTRY_COMPONENTS } from "./Components";
import { AppComponent } from "./Components/AppComponent";
import { AppRoutingModule, ROUTES_PROVIDERS  } from "./AppRoutingModule";
import { APP_SERVICES } from "./Services";

@NgModule({
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        ReactiveFormsModule,
        InfrastructureModule,
        AppRoutingModule,
        AgmCoreModule.forRoot({
            apiKey: "AIzaSyBpbiRV_0vM4Ied07zt1oFjfKnAhHZM3_I",
            libraries: ["places"]
        })
    ],
    declarations: [
        AppComponent,
        ...APP_COMPONENTS
    ],
    entryComponents: [
        ...APP_ENTRY_COMPONENTS
    ],
    providers: [
        { provide: ErrorHandler, useClass: ExceptionHandler },
        ...ROUTES_PROVIDERS,
        ...INFRASTRUCTURE_INJECTABLES,
        ...APP_SERVICES
    ],
    bootstrap: [ AppComponent ]
})
export class AppModule {}