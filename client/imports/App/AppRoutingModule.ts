import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { Meteor } from "meteor/meteor";

import { HomeComponent } from "./Components/HomeComponent";

export const routes: Routes = [
    { path: "", component: HomeComponent },
    { path: "home", component: HomeComponent }
];

export const ROUTES_PROVIDERS = [{
    provide: "canActivateForLoggedIn",
    useValue: () => !!Meteor.userId()
}];

@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
})
export class AppRoutingModule {}
