import { Utils as InfrastructureUtils } from "../../../Infrastructure";
export class Utils extends InfrastructureUtils {
    getMeteorSetting(settingKey: string): any {
        return Meteor.settings[settingKey];
    }
}