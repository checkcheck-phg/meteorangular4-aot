import { Component } from "@angular/core";

import { BaseComponent, CommonService } from "../../Infrastructure";

import template from "./AppComponent.html";

@Component({
    selector: "app",
    template
})
export class AppComponent extends BaseComponent {
    currentRoute: string;

    constructor(commonService: CommonService) {
        super(commonService);
    }
}
