﻿import { Component, ViewChild } from "@angular/core";

import { BaseComponent, CommonService } from "../../Infrastructure";

import template from "./HomeComponent.html";

@Component({
    selector: "home",
    template
})
export class HomeComponent extends BaseComponent {
    constructor(commonService: CommonService) {
        super(commonService);
    }
}