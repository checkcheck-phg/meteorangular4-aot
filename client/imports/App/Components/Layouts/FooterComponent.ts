import { Component } from "@angular/core";

import { BaseComponent, CommonService } from "../../../Infrastructure";

import template from "./FooterComponent.html";

@Component({
    selector: "footer",
    template
})
export class FooterComponent extends BaseComponent {
    isContentVisible = false;

    constructor(commonService: CommonService) {
        super(commonService);
    }

    private onMouseEnter(event): void {
        this.isContentVisible = true;
    }

    private onMouseLeave(event): void {
        this.isContentVisible = false;
        console.log("TEST");
    }
}
