import { Component } from "@angular/core";
import { MeteorObservable, ObservableCursor } from "meteor-rxjs";

import { BaseComponent, CommonService } from "../../../Infrastructure";

import template from "./HeaderComponent.html";

@Component({
    selector: "header",
    template
})
export class HeaderComponent extends BaseComponent {
    private languages = this.languageService.getLanguages();
    private languageCode = this.translateService.language.toUpperCase();
    public userName: string;

    constructor(commonService: CommonService) {
        super(commonService);
    }

    test(): void {
        this.modalService.notImplemented()
    }

    private switchLanguage(languageCode: string): void {
        this.languageCode = languageCode;
        this.translateService.language = languageCode.toLowerCase();
    }

    get languageImgSrc(): string {
        return `/assets/img/flags/${this.languageCode}.png`;
    }
}
