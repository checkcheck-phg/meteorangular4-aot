
import "reflect-metadata";
import { Component } from "@angular/core";

export function ModalComponent(annotation: Component): Function {
    return function (target: Function): void {
        let template = `<div md-dialog-title *ngIf="isTitleVisible"><h3>{{ title | translate }}</h3></div>
                        <md-dialog-content>
                            ${annotation.template}
                        </md-dialog-content>
                        <md-dialog-actions *ngIf="isFooterVisible" [attr.align]="center">
                            <ng-template ngFor let-footer [ngForOf]="footers">
                                <button md-raised-button
                                        *ngIf="isFooterButtonVisible(footer)"
                                        [type]="footer.type ? footer.type : ''"
                                        [disabled]="isFooterButtonDisabled(footer)"
                                        [ngClass]="footer.classes"
                                        (click)="clicked(footer)">
                                    {{ footer.title | translate }}
                                    <i class="{{ footer.classIcon }}"></i>
                                </button>
                            </ng-template>
                        </md-dialog-actions>`;
        annotation.template = template;
        var metadata = new Component(annotation);
        Reflect.defineMetadata("annotations", [ metadata ], target);
    }
}