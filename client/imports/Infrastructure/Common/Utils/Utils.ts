import { NgForm, NgModel } from "@angular/forms";

interface ActiveXObject {
    new (s: string): any;
}
declare var ActiveXObject: ActiveXObject;

export class Utils {
    static stringFormat(source: string, ...args: string[]): string {
        return source.replace(/{(\d+)}/g, (match, number) => (typeof args[number] !== "undefined" && args[number] !== null) ? args[number] : match);
    }

    static parseXML(str: string): any  {
        let xmlDoc;
        if (document.implementation && document.implementation.createDocument) {
            xmlDoc = new DOMParser().parseFromString(str, "text/xml");
        }
        else if ((<any>window).ActiveXObject) {
            xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
            xmlDoc.loadXML(str);
        }
        return xmlDoc;
    }

    static mergeNgModelValidatorToNgForm(source: NgModel, destination: NgForm): boolean {
        if (!_.isEmpty(destination.controls)) {
            let controlDestination = destination.controls[source.name];
            if (controlDestination) {
                controlDestination.setValidators(source.control.validator)
                controlDestination.setAsyncValidators(source.control.asyncValidator);
                setTimeout(() => controlDestination.setErrors(source.errors));
                return true;
            }
        }
        return false;
    }

    static mergeNgModelValidatorFromNgForm(destination: NgModel, source: NgForm): boolean {
        if (!_.isEmpty(source.controls)) {
            let controlSrc = source.controls[destination.name];
            if (controlSrc) {
                destination.control.setValidators(controlSrc.validator)
                destination.control.setAsyncValidators(controlSrc.asyncValidator);
                destination.control.setErrors(controlSrc.errors);
                return true;
            }
        }
        return false;
    }
}