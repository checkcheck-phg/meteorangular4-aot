import { OnInit, AfterViewInit, OnDestroy } from "@angular/core";
import { NgForm } from "@angular/forms";

import {
    CommonService, ModalService, LogService, TranslateService, CountryService,
    LanguageService, ToastrService, ReferenceDataService
} from "../Services";

import { IReferenceData } from "./../Interfaces/IReferenceData";
import { EResource } from "../Enums/EResource";
import { EReferenceDataType } from "../Enums/EReferenceDataType";

export abstract class BaseComponent implements OnInit, AfterViewInit, OnDestroy {
    readonly EResource = EResource;
    readonly EReferenceDataType = EReferenceDataType;

    protected form: NgForm = null;
    protected referenceData: IReferenceData;

    protected modalService: ModalService;
    protected logService: LogService;
    protected translateService: TranslateService;
    protected countryService: CountryService;
    protected languageService: LanguageService;
    protected toastrService: ToastrService;
    protected referenceDataService: ReferenceDataService;

    protected isReadOnly: boolean;
    protected isEditable: boolean;
    protected isVisible = true;

    constructor(protected commonService: CommonService) {
        if (commonService) {
            this.modalService = commonService.modalService;
            this.logService = commonService.logService;
            this.translateService = commonService.translateService;
            this.countryService = commonService.countryService;
            this.languageService = commonService.languageService;
            this.toastrService = commonService.toastrService;
            this.referenceDataService = commonService.referenceDataService;
        }
    }

    ngOnInit(): void {
        this.onInit();
    }

    ngAfterViewInit(): void {
        this.afterViewInit();
    }

    ngOnDestroy(): void {
        this.onDestroy();
    }

    protected onInit(): void {
        //virtual method
    }

    protected afterViewInit(): void {
        //virtual method
    }

    protected onDestroy(): void {
        //virtual method
    }
}