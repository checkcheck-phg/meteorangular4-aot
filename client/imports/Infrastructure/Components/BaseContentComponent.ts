import { ElementRef } from "@angular/core";

import { BaseComponent } from "./BaseComponent";
import { CommonService } from "../Services/CommonService";

export abstract class BaseContentComponent extends BaseComponent {
    constructor(protected element: ElementRef, commonService: CommonService) {
        super(commonService);
    }
}