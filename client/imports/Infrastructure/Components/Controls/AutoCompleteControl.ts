import { Component, ElementRef, ViewChild, Input, Optional, forwardRef } from "@angular/core";
import { NgForm, NgModel, FormControl, NG_VALUE_ACCESSOR, ControlValueAccessor } from "@angular/forms";
import { Observable } from "rxjs";

import "rxjs/add/operator/startWith";
import "rxjs/add/operator/map";

import { BaseControl } from "./BaseControl";
import { CommonService } from "../../Services/CommonService";
import { EReferenceDataType } from "../../Enums/EReferenceDataType";

@Component({
    selector: "autocomplete-control",
    template: `<md-input-container>
                   <input mdInput
                          #innerElement
                          #innerNgModel="ngModel"
                          [placeholder]="placeholder"
                          [mdAutocomplete]="auto"
                          [(ngModel)]="value"
                          [disabled]="disabled"
                          [required]="required"
                          (blur)="onBlur($event)"
                          (focus)="onFocus($event)">
               </md-input-container>
               <md-autocomplete #auto="mdAutocomplete">
                   <md-option *ngFor="let item of listFiltered | async" [value]="getOptionValue(item)">
                       {{ getOptionText(item) }}
                   </md-option>
               </md-autocomplete>`,
    providers: [{
        provide: NG_VALUE_ACCESSOR,
        useExisting: forwardRef(() => AutoCompleteControl),
        multi: true
    }]
})
export class AutoCompleteControl extends BaseControl<any> {
    @Input() private placeholder: string;
    @Input() private list: any[] = [];
    @Input() private textField: string;
    @Input() private valueField: string;
    @Input() private referenceDataType: EReferenceDataType;

    private listFiltered: Observable<any>;

    constructor(@Optional() ngForm: NgForm, element: ElementRef, commonService: CommonService) {
        super(ngForm, element, commonService);
    }

    protected onInit(): void {
        if (!_.isUndefined(this.referenceDataType)) {
            this.textField = "Text";
            this.valueField = "Code";
            this.commonService.referenceDataService.loadReferenceData(
                [this.referenceDataType],
                (referenceData) => this.list = referenceData[EReferenceDataType[this.referenceDataType]]
            );
        }

        this.commonService.translateService.translate(this.placeholder)
            .subscribe(result => this.placeholder = result[this.placeholder]);

        this.listFiltered = this.innerNgModel.valueChanges
            .map(this.filter.bind(this));
    }

    private filter(value: string) {
        if (_.isEmpty(value)) {
            return this.list;
        }

        return this.list.filter(item => {
            if (_.isObject(item)) {
                let regx = new RegExp(`^${value}`, "gi");
                return regx.test(item[this.valueField]) || regx.test(item[this.textField]);
            }
            return new RegExp(`^${value}`, "gi").test(item);
        });
    }

    private getOptionValue(item: any): any {
        return _.isObject(item) ? item[this.valueField] : item;
    }

    private getOptionText(item: any): any {
        return _.isObject(item) ? item[this.textField] : item;
    }
}