import { ElementRef, ViewChild, Input, HostBinding, OnInit, AfterViewInit, AfterViewChecked, OnDestroy } from "@angular/core";
import { NgForm, NgModel, ControlValueAccessor } from "@angular/forms";

import { Utils } from "./../../Common/Utils/Utils";
import { CommonService } from "../../Services/CommonService";
import { EResource } from "../../Enums/EResource";

const noop = () => {};

export abstract class BaseControl<TValue> implements OnInit, AfterViewInit, AfterViewChecked, OnDestroy, ControlValueAccessor {
    @HostBinding("class") class = "form-control";

    @ViewChild("innerElement") protected innerElement: ElementRef;
    @ViewChild("innerNgModel") protected innerNgModel: NgModel;

    @Input() disabled: boolean | Function;
    @Input() readOnly: boolean | Function;
    @Input() required;

    readonly EResource = EResource;

    private _value: TValue;
    private onTouchedCallback: () => void = noop;
    private onChangeCallback: (_: any) => void = noop;

    protected validatorMerged: boolean;
    protected focused: boolean;

    constructor(public form: NgForm,
        protected element: ElementRef,
        protected commonService: CommonService
    ) {
        this.element.nativeElement.componentInstance = this;
    }

    addClass(addClass: string): void {
        this.class = _.compact(_.uniq([
            ...this.class.split("_"),
            ...addClass.split("_")
        ])).join(" ");
    }

    ngOnInit(): void {
        this.addClass(this.element.nativeElement.className);
        this.innerNgModel.name = this.element.nativeElement.getAttribute("name");
        this.onInit();
    }

    ngAfterViewInit(): void {
        this.afterViewInit();
    }

    ngAfterViewChecked(): void {
        this.afterViewChecked();
    }

    ngOnDestroy(): void {
        this.onDestroy();
    }

    writeValue(v: any): void {
        this.value = v;
    }

    registerOnChange(fn: any): void {
        this.onChangeCallback = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouchedCallback = fn;
    }

    protected onBlur(event: FocusEvent): void {
        if (event.relatedTarget && this.element.nativeElement.contains(event.relatedTarget)) {
            event.preventDefault();
            return;
        }
        this.focused = false;
        this.onTouchedCallback();
    }

    protected onFocus(): void {
        this.focused = true;
    }

    protected onInit(): void {
        //virtual method
    }

    protected afterViewInit(): void {
        //virtual method
    }

    protected afterViewChecked(): void {
        if (!this.validatorMerged) {
            this.validatorMerged = Utils.mergeNgModelValidatorFromNgForm(this.innerNgModel, this.form);
        }
    }

    protected onDestroy(): void {
        //virtual method
    }

    get messageError(): string {
        if (!this.innerNgModel.name) {
            return;
        }

        return [
            this.innerNgModel.name.split("_").shift(),
            s.capitalize(Object.keys(this.innerNgModel.errors).shift())
        ].join("_");
    }

    get value(): any {
        return this._value;
    };

    set value(v: any) {
        if (v !== this._value) {
            this._value = v;
            this.onChangeCallback(v);
        }
    }

    get isDisabled(): boolean {
        return _.isFunction(this.disabled) ? this.disabled() : this.disabled;
    }

    get isReadOnly(): boolean {
        return _.isFunction(this.readOnly) ? this.readOnly() : this.readOnly;
    }
}