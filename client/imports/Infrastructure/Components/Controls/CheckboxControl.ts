import { Component, ElementRef, ViewChild, Input, Optional, forwardRef } from "@angular/core";
import { NgForm, NgModel, FormControl, NG_VALUE_ACCESSOR, ControlValueAccessor } from "@angular/forms";

import { BaseControl } from "./BaseControl";
import { CommonService } from "../../Services/CommonService";

@Component({
    selector: "checkbox-control",
    template: `<md-checkbox #innerElement
                            #innerNgModel="ngModel"
                            [(ngModel)]="value"
                            [checked]="value"
                            [indeterminate]="indeterminate"
                            [disabled]="isDisabled"
                            (blur)="onBlur($event)"
                            (focus)="onFocus($event)">
                   {{ label }}
               </md-checkbox>`,
    providers: [{
        provide: NG_VALUE_ACCESSOR,
        useExisting: forwardRef(() => CheckboxControl),
        multi: true
    }]
})
export class CheckboxControl extends BaseControl<string> {
    @Input() private label;

    constructor(@Optional() ngForm: NgForm, element: ElementRef, commonService: CommonService) {
        super(ngForm, element, commonService);
    }
}