import { Component, ElementRef, ViewChild, Input, Optional, forwardRef } from "@angular/core";
import { NgForm, NgModel, FormControl, NG_VALUE_ACCESSOR, ControlValueAccessor } from "@angular/forms";

import { BaseControl } from "./BaseControl";
import { CommonService } from "../../Services/CommonService";

@Component({
    selector: "date-control",
    template: `<md-input-container>
                   <input mdInput
                          #innerElement
                          #innerNgModel="ngModel"
                          [mdDatepicker]="picker"
                          [placeholder]="placeholder"
                          [disabled]="disabled"
                          [(ngModel)]="value"
                          [required]="required"
                          (blur)="onBlur($event)"
                          (focus)="onFocus($event)">
                   <button mdSuffix [mdDatepickerToggle]="picker"></button>
               </md-input-container>
               <md-datepicker #picker></md-datepicker>`,
    providers: [{
        provide: NG_VALUE_ACCESSOR,
        useExisting: forwardRef(() => DateControl),
        multi: true
    }]
})
export class DateControl extends BaseControl<string> {
    @Input() private placeholder: string;
    private touch: boolean;

    constructor(@Optional() ngForm: NgForm, element: ElementRef, commonService: CommonService) {
        super(ngForm, element, commonService);
    }

    protected onInit(): void {
        this.commonService.translateService.translate(this.placeholder)
            .subscribe(result => this.placeholder = result[this.placeholder]);
    }
}