import { Component, ElementRef, ViewChild, Input, Output, EventEmitter, Optional, forwardRef } from "@angular/core";
import { NgForm, NgModel, FormControl, NG_VALUE_ACCESSOR, ControlValueAccessor } from "@angular/forms";

import { Utils } from "./../../Common/Utils/Utils";
import { BaseControl } from "./BaseControl";
import { CommonService } from "../../Services/CommonService";

@Component({
    selector: "password-control",
    template: `<md-input-container>
                   <div fxLayout="row">
                       <button fxFlexAlign="start"
                               class="btn-show-password"
                               [ngClass]="type"
                               md-icon-button
                               *ngIf="showIconShowPassword"
                               (click)="showPassword()">
                           <md-icon>remove_red_eye</md-icon>
                       </button>
                       <input fxFlex
                              mdInput
                              #innerElement
                              #innerNgModel="ngModel"
                              [type]="type"
                              [placeholder]="placeholder"
                              [disabled]="disabled"
                              [(ngModel)]="value"
                              (blur)="onBlur($event)"
                              (focus)="onFocus($event)"
                              maxlength="50"
                              spellcheck="false"
                              required />
                       <button fxFlexAlign="end"
                               *ngIf="!this.focused && showForgotPassword"
                               class="btn-link"
                               (click)="forgotPassword()">{{ 'Forgot_Password_Text' | translate }}</button>
                       <ng2-password-strength-bar fxFlexAlign="end"
                                                  *ngIf="!showForgotPassword"
                                                  [passwordToCheck]="value"></ng2-password-strength-bar>
                       <md-error *ngIf="innerNgModel.invalid && innerNgModel.errors.required">{{ messageError | translate }}</md-error>
                   </div>
               </md-input-container>`,
    providers: [{
        provide: NG_VALUE_ACCESSOR,
        useExisting: forwardRef(() => PasswordControl),
        multi: true
    }]
})
export class PasswordControl extends BaseControl<string> {
    @Input() hasForgotPassword;
    @Output() onForGotPasswordClick: EventEmitter<void> = new EventEmitter<void>();
    private placeholder = "Field.Password";
    private type = "password";

    constructor(@Optional() ngForm: NgForm, element: ElementRef, commonService: CommonService) {
        super(ngForm, element, commonService);
    }

    private showPassword(): void {
        this.type = this.type === "password" ? "text" : "password";
    }

    private forgotPassword(): void {
        //this.commonService.toastrService.notImplemented();
        this.onForGotPasswordClick.emit();
    }

    protected onInit(): void {
        if (this.showForgotPassword) {
            this.addClass("has-forgot-password");
        }
        this.commonService.translateService.translate(this.placeholder)
            .subscribe(result => this.placeholder = result[this.placeholder]);
    }

    protected afterViewChecked(): void {
        if (!this.validatorMerged) {
            this.validatorMerged = Utils.mergeNgModelValidatorToNgForm(this.innerNgModel, this.form);
        }
    }

    get showForgotPassword(): boolean {
        return !_.isUndefined(this.hasForgotPassword);
    }

    get showIconShowPassword(): boolean {
        return this.focused || !_.isEmpty(this.value);
    }
}