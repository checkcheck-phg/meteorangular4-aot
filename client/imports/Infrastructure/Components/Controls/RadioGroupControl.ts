import { Component, ElementRef, ViewChild, Input, Optional, forwardRef } from "@angular/core";
import { NgForm, NgModel, FormControl, NG_VALUE_ACCESSOR, ControlValueAccessor } from "@angular/forms";
import { Observable, Subject } from "rxjs";

import { BaseControl } from "./BaseControl";
import { CommonService } from "../../Services/CommonService";
import { EReferenceDataType } from "../../Enums/EReferenceDataType";

@Component({
    selector: "radio-group-control",
    template: `<md-radio-group #innerElement
                               #innerNgModel="ngModel"
                               [(ngModel)]="value"
                               [disabled]="disabled"
                               [required]="required"
                               (blur)="onBlur($event)"
                               (focus)="onFocus($event)">
                   <md-radio-button *ngFor="let item of list" [value]="item[valueField]">
                       {{ item[textField] }}
                   </md-radio-button>
               </md-radio-group>`,
    providers: [{
        provide: NG_VALUE_ACCESSOR,
        useExisting: forwardRef(() => RadioGroupControl),
        multi: true
    }]
})
export class RadioGroupControl extends BaseControl<string> {
    @Input() private vertical: boolean;
    @Input() private list: any[] = [];
    @Input() private textField: string;
    @Input() private valueField: string;
    @Input() private referenceDataType: EReferenceDataType;

    private listSubject: Subject<any> = new Subject<any>()
    private listObservable = this.listSubject.asObservable()

    constructor(@Optional() ngForm: NgForm, element: ElementRef, commonService: CommonService) {
        super(ngForm, element, commonService);
    }

    protected onInit(): void {
        if (!_.isUndefined(this.vertical)) {
            this.addClass("form-control-vertical")
        }
        if (!_.isUndefined(this.referenceDataType)) {
            this.textField = "Text";
            this.valueField = "Code";
            this.commonService.referenceDataService.loadReferenceData(
                [this.referenceDataType],
                (referenceData) => this.list = referenceData[EReferenceDataType[this.referenceDataType]]
            );
        }
    }
}