import { Component, ElementRef, ViewChild, Input, Optional, forwardRef } from "@angular/core";
import { NgForm, NgModel, FormControl, NG_VALUE_ACCESSOR, ControlValueAccessor } from "@angular/forms";
import { Observable, Subject } from "rxjs";

import { BaseControl } from "./BaseControl";
import { CommonService } from "../../Services/CommonService";
import { EReferenceDataType } from "../../Enums/EReferenceDataType";

@Component({
    selector: "select-control",
    template: `<md-select #innerElement
                          #innerNgModel="ngModel"
                          [placeholder]="placeholder"
                          [(ngModel)]="value"
                          [disabled]="disabled"
                          [required]="required"
                          (blur)="onBlur($event)"
                          (focus)="onFocus($event)">
                   <md-option *ngFor="let item of list" [value]="getOptionValue(item)">
                       {{ getOptionText(item) }}
                   </md-option>
               </md-select>`,
    providers: [{
        provide: NG_VALUE_ACCESSOR,
        useExisting: forwardRef(() => SelectControl),
        multi: true
    }]
})
export class SelectControl extends BaseControl<string> {
    @Input() private placeholder: string;
    @Input() private list: any[] = [];
    @Input() private textField: string;
    @Input() private valueField: string;
    @Input() private referenceDataType: EReferenceDataType;

    private listSubject: Subject<any> = new Subject<any>()
    private listObservable = this.listSubject.asObservable()

    constructor(@Optional() ngForm: NgForm, element: ElementRef, commonService: CommonService) {
        super(ngForm, element, commonService);
    }

    protected onInit(): void {
        this.commonService.translateService.translate(this.placeholder)
            .subscribe(result => this.placeholder = result[this.placeholder]);

        if (!_.isUndefined(this.referenceDataType)) {
            this.textField = "Text";
            this.valueField = "Code";
            this.commonService.referenceDataService.loadReferenceData(
                [this.referenceDataType],
                (referenceData) => this.list = referenceData[EReferenceDataType[this.referenceDataType]]
            );
        }
    }

    private getOptionValue(item: any): any {
        return _.isObject(item) ? item[this.valueField] : item;
    }

    private getOptionText(item: any): any {
        return _.isObject(item) ? item[this.textField] : item;
    }
}