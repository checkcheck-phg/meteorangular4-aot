import { Component, ElementRef, ViewChild, Input, Optional, forwardRef } from "@angular/core";
import { NgForm, NgModel, FormControl, NG_VALUE_ACCESSOR, ControlValueAccessor } from "@angular/forms";

import { BaseControl } from "./BaseControl";
import { CommonService } from "../../Services/CommonService";

@Component({
    selector: "slide-toggle-control",
    template: `<md-slide-toggle #innerElement
                                #innerNgModel="ngModel"
                                [checked]="value"
                                [(ngModel)]="value"
                                [disabled]="isDisabled"
                                (blur)="onBlur($event)"
                                (focus)="onFocus($event)">
                            {{ label }}
               </md-slide-toggle>`,
    providers: [{
        provide: NG_VALUE_ACCESSOR,
        useExisting: forwardRef(() => SlideToggleControl),
        multi: true
    }]
})
export class SlideToggleControl extends BaseControl<string> {
    @Input() private label;

    constructor(@Optional() ngForm: NgForm, element: ElementRef, commonService: CommonService) {
        super(ngForm, element, commonService);
    }
}