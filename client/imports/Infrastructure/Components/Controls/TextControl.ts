import { Component, ElementRef, ViewChild, Input, Optional, forwardRef } from "@angular/core";
import { NgForm, NgModel, FormControl, NG_VALUE_ACCESSOR, ControlValueAccessor } from "@angular/forms";

import { BaseControl } from "./BaseControl";
import { CommonService } from "../../Services/CommonService";

@Component({
    selector: "text-control",
    template: `<md-input-container>
                   <input mdInput
                          #innerElement
                          #innerNgModel="ngModel"
                          [type]="type"
                          [placeholder]="placeholder"
                          [disabled]="disabled"
                          [(ngModel)]="value"
                          spellcheck="false"
                          [required]="required"
                          (blur)="onBlur($event)"
                          (focus)="onFocus($event)">
                   <ng-content></ng-content>
                   <md-error *ngIf="innerNgModel.invalid && innerNgModel.errors.required">{{ 'Validation.'+ messageError | translate: EResource.Message }}</md-error>
                   <md-error *ngIf="innerNgModel.invalid && innerNgModel.errors.email" >{{ 'Validation.'+ messageError | translate: EResource.Message }}</md-error>
               </md-input-container>`,
    providers: [{
        provide: NG_VALUE_ACCESSOR,
        useExisting: forwardRef(() => TextControl),
        multi: true
    }]
})
export class TextControl extends BaseControl<string> {
    @Input() private placeholder: string;
    @Input() private type = "text";

    constructor(@Optional() ngForm: NgForm, element: ElementRef, commonService: CommonService) {
        super(ngForm, element, commonService);
    }

    protected onInit(): void {
        this.commonService.translateService.translate(this.placeholder)
            .subscribe(result => this.placeholder = result[this.placeholder]);
    }
}