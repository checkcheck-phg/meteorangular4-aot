import { ElementRef } from "@angular/core";
import { NgForm } from "@angular/forms";
import { MdDialogRef, MdDialogContainer } from "@angular/material";

import { BaseComponent } from "../BaseComponent";
import { CommonService } from "../../Services/CommonService";
import { EModalSize } from "../../Enums/EModalSize";
import { IModalButtonFooter } from "../../Interfaces/IModalButtonFooter";

export abstract class BaseModalComponent<T> extends BaseComponent {
    static size: EModalSize | number = EModalSize.MEDIUM;
    classes: string;
    title: string;

    footers: IModalButtonFooter[] = [{
        title: "Button.Cancel_Text",
        callback: this.close.bind(this)
    }];

    constructor(protected dialogRef: MdDialogRef<T>, commonService?: CommonService) {
        super(commonService);
    }

    close(): void {
        this.dialogRef.close();
    }

    clicked(button: IModalButtonFooter): void {
        if (button.callback) {
            button.callback();
        }
    }

    isFooterButtonVisible(button: IModalButtonFooter): boolean {
        return !button.visible || (_.isFunction(button.visible) ? (<Function>button.visible)() : button.visible);
    }

    isFooterButtonDisabled(button: IModalButtonFooter): boolean {
        return button.disabled && _.isFunction(button.disabled) ? (<Function>button.disabled)() : button.disabled;
    }

    protected afterViewInit(): void {
        let containerRef = <ElementRef>(<any>this.dialogRef._containerInstance)._elementRef;
        jQuery(containerRef.nativeElement).draggable({ handle: "[md-dialog-title]" });
    }

    protected updateSize(width: number | EModalSize, height?: number | EModalSize): void {
        this.dialogRef.updateSize(width ? `${width}px` : null, height ? `${height}px` : null);
    }

    get isFooterVisible(): boolean {
        return !_.isEmpty(this.footers);
    }

    get isTitleVisible(): boolean {
        return !_.isEmpty(this.title);
    }

}