import { ElementRef, HostBinding } from "@angular/core";
import { NgForm } from "@angular/forms";
import { MdDialogRef } from "@angular/material";

import { ModalComponent } from "../../Annotations/ModalComponent";
import { BaseModalComponent } from "./BaseModalComponent";
import { CommonService } from "../../Services/CommonService";
import { EModalSize } from "../../Enums/EModalSize";
import { EResource } from "../../Enums/EResource";

import template from "./MsgDialogComponent.html";

@ModalComponent({
    selector: "msg-dialog",
    template,
})
export class BaseMsgDialogComponent<T> extends BaseModalComponent<T> {
    @HostBinding("attr.class") classes = "r3s-msg-dialog";
    static size = EModalSize.SMALL;
    gridColumns = 6;
    message: string;

    constructor(dialogRef: MdDialogRef<T>) {
        super(dialogRef);
    }
}