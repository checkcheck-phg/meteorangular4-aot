import { Component } from "@angular/core";

import { BaseMsgDialogComponent } from "./BaseMsgDialogComponent";
import { IModalButtonFooter } from "../../Interfaces/IModalButtonFooter";

export class ConfirmDialogComponent extends BaseMsgDialogComponent<ConfirmDialogComponent> {
    classes = "r3s-msg-dialog-confirm";
    title = "Confirm";
    icon = "check_circle";

    ok(): void {
        this.dialogRef.close(true);
    }

    protected onInit(): void {
        this.footers = [<IModalButtonFooter>{
            title: "OK",
            callback: this.ok.bind(this)
        }].concat(this.footers);
    }
}