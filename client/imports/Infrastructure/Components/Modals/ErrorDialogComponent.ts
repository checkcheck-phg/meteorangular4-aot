import { Component } from "@angular/core";

import { BaseMsgDialogComponent } from "./BaseMsgDialogComponent";

export class ErrorDialogComponent extends BaseMsgDialogComponent<ErrorDialogComponent> {
    classes = "r3s-msg-dialog-error";
    title = "Error";
    icon = "error";
    isException = false;
}