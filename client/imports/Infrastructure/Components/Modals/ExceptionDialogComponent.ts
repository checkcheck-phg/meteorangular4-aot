import { Component } from "@angular/core";

import { ErrorDialogComponent } from "./ErrorDialogComponent";
import { EModalSize } from "../../Enums/EModalSize";

export class ExceptionDialogComponent extends ErrorDialogComponent {
    static size = 1000;
    title = "Exception";
    isException = true;
}