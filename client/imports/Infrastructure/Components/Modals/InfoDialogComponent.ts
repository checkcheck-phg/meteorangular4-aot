import { Component } from "@angular/core";

import { BaseMsgDialogComponent } from "./BaseMsgDialogComponent";

export class InfoDialogComponent extends BaseMsgDialogComponent<InfoDialogComponent> {
    classes = "r3s-msg-dialog-info";
    title = "Infomations";
    icon = "info";
}