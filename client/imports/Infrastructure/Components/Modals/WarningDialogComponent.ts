import { Component } from "@angular/core";

import { BaseMsgDialogComponent } from "./BaseMsgDialogComponent";

export class WarningDialogComponent extends BaseMsgDialogComponent<WarningDialogComponent> {
    classes = "r3s-msg-dialog-warning";
    title = "Warning";
    icon = "warning";
}