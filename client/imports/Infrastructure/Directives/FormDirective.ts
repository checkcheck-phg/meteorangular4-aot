import { Directive, HostBinding, Injector, AfterViewInit } from "@angular/core";
import { NgForm } from "@angular/forms";

@Directive({ selector: "form" })
export class FormDirective implements AfterViewInit {
    @HostBinding("class") class = "form-horizontal";
    @HostBinding("attr.novalidate") novalidate = "";

    private oldSubmit: { ($event: Event): boolean };

    constructor(private form: NgForm, injector: Injector) {
        let parentComponent = (<any>injector).view.context;
        parentComponent.form = form;
    }

    ngAfterViewInit(): void {
        this.oldSubmit = this.form.onSubmit;
        this.form.onSubmit = this.onSubmit.bind(this);
    }

    onSubmit(event: Event): boolean {
        if (this.form.valid) {
            try {
                this.oldSubmit.apply(this.form, [event]);
                this.form.form["_pristine"] = true;
            } catch (ex) {
                this.form.form["_pristine"] = false;
                throw ex;
            }
        }
        return false;
    }
}