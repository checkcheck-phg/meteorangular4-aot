import { Directive, ElementRef, ViewContainerRef } from "@angular/core";
import { NgModel } from "@angular/forms";

const monkeyPatchingMethodName = "_setUpControl";

@Directive({ selector: "[ngModel]" })
export class NgModelDirective {
    constructor(element: ElementRef, ngModel: NgModel) {
        let name = element.nativeElement.getAttribute("name");
        if (name) {
            name += `_${uuid.v4()}`;
            element.nativeElement.setAttribute("name", name);
        } else if (element.nativeElement.parentNode) {
            name = element.nativeElement.parentNode.getAttribute("name");
        }

        let oldAddControl = ngModel[monkeyPatchingMethodName];
        ngModel[monkeyPatchingMethodName] = () => {
            ngModel.name = name || ngModel.name;
            oldAddControl.apply(ngModel);
        };
    }
}