import { Directive, ElementRef, Input, AfterViewInit } from "@angular/core";

import { CommonService } from "./../Services/CommonService";
import { EResource } from "./../Enums/EResource";

@Directive({ selector: "[translate]" })
export class TranslateDirective implements AfterViewInit {
    @Input() translate: string;

    private lastInnerText;

    constructor(private commonService: CommonService, private element: ElementRef) {}

    ngAfterViewInit(): void {
        let innerTextKey = this.element.nativeElement.innerText;
        let titleKey = this.element.nativeElement.title;
        let placeholderKey = this.element.nativeElement.getAttribute("placeholder");
        let keys = _.compact([innerTextKey, titleKey, placeholderKey]);
        keys.forEach((k, i) => keys[i] = k.trim());
        this.translateHandler(keys, innerTextKey, titleKey, placeholderKey);
        this.commonService.translateService.languageChanged.subscribe(this.translateHandler.bind(this, keys, innerTextKey, titleKey, placeholderKey));
    }

    private translateHandler(keys: string[], innerTextKey: string, titleKey: string, placeholderKey: string): void {
        this.commonService.translateService.translate(_.union(keys), this.translate ? EResource[this.translate] : null)
            .subscribe(results => {
                innerTextKey = _.isEmpty(innerTextKey) ? null : innerTextKey.trim();
                if (results[innerTextKey]) {
                    let replaceStr = this.lastInnerText || innerTextKey;
                    this.element.nativeElement.innerHTML = this.element.nativeElement.innerHTML.replace(replaceStr, results[innerTextKey]);
                    this.lastInnerText = results[innerTextKey];
                }

                titleKey = _.isEmpty(titleKey) ? null : titleKey.trim();
                if (results[titleKey]) {
                    this.element.nativeElement.title = results[titleKey];
                }

                placeholderKey = _.isEmpty(placeholderKey) ? null : placeholderKey.trim();
                if (results[placeholderKey]) {
                    this.element.nativeElement.setAttribute("placeholder", results[placeholderKey]);
                }
            });
    }
}