export enum EModalSize {
    LARGE = 1400,
    MEDIUM = 800,
    SMALL = 400
}