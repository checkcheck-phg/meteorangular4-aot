export enum EToastrType {
    ERROR,
    INFO,
    SUCCESS,
    WARNING
}