import { Type, NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpModule, Http } from "@angular/http";
import { FlexLayoutModule } from "@angular/flex-layout";

import { MaterialModule, OverlayContainer, FullscreenOverlayContainer, MdDatepickerModule, MdNativeDateModule } from "@angular/material";

import { AgmCoreModule } from "angular2-google-maps/core";
import { ChartsModule } from "ng2-charts";
import { PerfectScrollbarModule } from "ngx-perfect-scrollbar";
import { ReCaptchaModule } from "angular2-recaptcha";
import { CustomFormsModule } from "ng2-validation";
import { PasswordStrengthBarModule } from "ng2-password-strength-bar"

import { ConfirmDialogComponent } from "./Components/Modals/ConfirmDialogComponent";
import { ErrorDialogComponent } from "./Components/Modals/ErrorDialogComponent";
import { WarningDialogComponent } from "./Components/Modals/WarningDialogComponent";
import { InfoDialogComponent } from "./Components/Modals/InfoDialogComponent";
import { ExceptionDialogComponent } from "./Components/Modals/ExceptionDialogComponent";

import { TextControl } from "./Components/Controls/TextControl";
import { PasswordControl } from "./Components/Controls/PasswordControl";
import { DateControl } from "./Components/Controls/DateControl";
import { SelectControl } from "./Components/Controls/SelectControl";
import { AutoCompleteControl } from "./Components/Controls/AutoCompleteControl";
import { RadioGroupControl } from "./Components/Controls/RadioGroupControl";
import { CheckboxControl } from "./Components/Controls/CheckboxControl";
import { SlideToggleControl } from "./Components/Controls/SlideToggleControl";

import { TranslateDirective } from "./Directives/TranslateDirective";
import { FormDirective } from "./Directives/FormDirective";
import { NgModelDirective } from "./Directives/NgModelDirective";

import { SafeHtmlPipe } from "./Pipes/SafeHtmlPipe";
import { TranslatePipe } from "./Pipes/TranslatePipe";

// External modules
const INFRASTRUCTURE_EXTERNAL_MODULES = [
    MaterialModule,
    FlexLayoutModule,
    PerfectScrollbarModule,
    ChartsModule,
    ReCaptchaModule,
    CustomFormsModule,
    PasswordStrengthBarModule,

    MdDatepickerModule,
    MdNativeDateModule
];

// entryComponents
export const INFRASTRUCTURE_ENTRY_COMPONENTS: Type<any>[] = [
    ConfirmDialogComponent,
    ErrorDialogComponent,
    WarningDialogComponent,
    InfoDialogComponent,
    ExceptionDialogComponent,

    TextControl,
    PasswordControl,
    DateControl,
    SelectControl,
    AutoCompleteControl,
    RadioGroupControl,
    CheckboxControl,
    SlideToggleControl
];

// Components
export const INFRASTRUCTURE_COMPONENTS_DIRECTIVES: Type<any>[] = [
    ...INFRASTRUCTURE_ENTRY_COMPONENTS,
    TranslateDirective,
    FormDirective,
    NgModelDirective
];

export const INFRASTRUCTURE_PIPES: Type<any>[] = [
    SafeHtmlPipe,
    TranslatePipe
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        HttpModule,
        FormsModule,
        ...INFRASTRUCTURE_EXTERNAL_MODULES
    ],
    declarations: [
        ...INFRASTRUCTURE_COMPONENTS_DIRECTIVES,
        ...INFRASTRUCTURE_PIPES
    ],
    entryComponents: [
        ...INFRASTRUCTURE_ENTRY_COMPONENTS
    ],
    providers: [
        { provide: OverlayContainer, useClass: FullscreenOverlayContainer }
    ],
    exports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        ...INFRASTRUCTURE_EXTERNAL_MODULES,
        ...INFRASTRUCTURE_COMPONENTS_DIRECTIVES,
        ...INFRASTRUCTURE_PIPES
    ]
})
export class InfrastructureModule {}
