export interface ILanguage {
    Code: string;
    Name: string;
}