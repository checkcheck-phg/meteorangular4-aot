export interface IModalButtonFooter {
    title: string;
    type?: string;
    classes?: string;
    classIcon?: string;
    callback?: Function;
    disabled?: boolean | Function;
    visible?: boolean | Function;
}