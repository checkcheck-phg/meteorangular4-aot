import { ReferenceDataValue } from "../../../../both";

export interface IReferenceData {
    [key: string]: ReferenceDataValue[];
}