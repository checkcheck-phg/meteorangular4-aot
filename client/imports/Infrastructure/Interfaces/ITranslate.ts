export interface ITranslate {
    Key: string;
    Text: string;
    Translation?: string;
}