import { Injectable, NgZone } from "@angular/core";
import { Meteor } from "meteor/meteor";
import { Accounts } from "meteor/accounts-base";
import { Tracker } from "meteor/tracker";

import { BaseModel } from "./BaseModel";

@Injectable()
export class Authentication<TProfile> extends BaseModel {
    private autorunComputation: Tracker.Computation;

    Username: string;
    Email: string;
    Phone: string;
    Password: string;
    ConfirmPassword: string;
    Profile: TProfile = <TProfile>{};

    constructor(private zone: NgZone) {
        super();
        this.initAutorun();
    }

    reset(): void {
        if (Meteor.user()) {
            this.Password = null;
            this.ConfirmPassword = null;
            this.Profile = _.extend(this.Profile, Meteor.user().profile);
            this.Id = Meteor.userId();
        } else {
            this.Username = null;
            this.Email = null;
            this.Password = null;
            this.ConfirmPassword = null;
            this.Profile = <TProfile>{};
            this.Id = null;
        }
    }

    signout(): void {
        Accounts.logout((error) => this.reset());
    }

    private initAutorun(): void {
        this.autorunComputation = Tracker.autorun(() => this.zone.run(this.reset.bind(this)));
    }

    get isAuthenticated(): boolean {
        return !_.isEmpty(this.Id);
    }
}