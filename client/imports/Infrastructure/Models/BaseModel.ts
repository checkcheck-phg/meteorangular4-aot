export abstract class BaseModel {
    private _id: string;

    get Id(): string {
        return this._id;
    }

    set Id(id: string) {
        this._id = id;
    }

    get IsNew(): boolean {
        return !this.Id;
    }
}