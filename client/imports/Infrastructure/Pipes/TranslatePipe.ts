import { Pipe, PipeTransform } from "@angular/core";

import { CommonService } from "./../Services/CommonService";
import { EResource } from "./../Enums/EResource";

@Pipe({
    name: "translate",
    pure: false
})
export class TranslatePipe implements PipeTransform {
    private resource: EResource;
    private key;
    private text = "";

    constructor(private commonService: CommonService) {}

    transform(value: string, resource?: EResource) {
        this.key = value;
        if (_.isEmpty(this.text)) {
            this.resource = resource;
            this.translateHandler()
            this.commonService.translateService.languageChanged.subscribe(this.translateHandler.bind(this));
        }
        return this.text;
    }

    private translateHandler(): void {
        this.commonService.translateService.translate(this.key, this.resource)
            .subscribe(results => {
                this.text = results[this.key];
            });
    }
}