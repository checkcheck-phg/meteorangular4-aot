import { MeteorObservable } from "meteor-rxjs";
import { Observable, Subscription } from "rxjs";

import { ModalService } from "../Services/ModalService";

export abstract class BaseModelService<T> {
    protected subscription: Subscription;

    constructor(protected methodPrefix, protected modalService: ModalService) {}

    unsubscribeData(): void {
        if(this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    get(id: string): Observable<T> {
        // Not implemented
        return null;
    }

    call<R>(method: string, data: any, callback: (response: R) => void): void {
        MeteorObservable.call<T>(`${this.methodPrefix}_${method}`, data)
            .subscribe(response => callback(<any>response), error => this.modalService.exception(error.message));
    }

    subscribe(name: string, data: any, callback: (response) => void): void {
        MeteorObservable.subscribe<T>(`${this.methodPrefix}_${name}`, data)
            .subscribe(response => callback(response), error => this.modalService.exception(error.message));
    }
}