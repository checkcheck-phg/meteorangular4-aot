import { Injectable  } from "@angular/core";

import { ModalService } from "./ModalService";
import { LogService } from "../../../../both";
import { TranslateService } from "./TranslateService";
import { CountryService } from "./CountryService";
import { LanguageService } from "./LanguageService";
import { ToastrService } from "./ToastrService";
import { MsTranslatorService } from "./MsTranslatorService";
import { ReferenceDataService } from "./ReferenceDataService";

@Injectable()
export class CommonService {
    constructor(public modalService: ModalService,
        public logService: LogService,
        public translateService: TranslateService,
        public countryService: CountryService,
        public languageService: LanguageService,
        public toastrService: ToastrService,
        public msTranslatorService: MsTranslatorService,
        public referenceDataService: ReferenceDataService) {}
}