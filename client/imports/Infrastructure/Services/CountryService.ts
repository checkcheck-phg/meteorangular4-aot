import { Injectable } from "@angular/core";

@Injectable()
export class CountryService {
    private countries = [
        { Code: "VN" },
        { Code: "CH" },
        { Code: "US" }
    ];

    getCountries(): any {
        return this.countries;
    }
}




