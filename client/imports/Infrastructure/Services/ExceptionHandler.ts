import { Injectable, ErrorHandler, Injector } from "@angular/core";

import { LogService } from "../../../../both";
import { ModalService } from "./ModalService";
import { CommonService } from "./CommonService";

@Injectable()
export class ExceptionHandler implements ErrorHandler {
    private modalService: ModalService;
    constructor(private logService: LogService, private injector: Injector) {}

    handleError(error): void {
        this.logService.error(error);
        if (!this.modalService) {
            this.modalService = this.injector.get(ModalService);
        }
        this.modalService.exception(error.stack);
    }
}