import { Injectable } from "@angular/core";

import { ILanguage } from "./../Interfaces/ILanguage";

@Injectable()
export class LanguageService {
    private languages: ILanguage[] = [
        { Code: "vi", Name: "Tiếng Việt" },
        { Code: "en", Name: "English" },
        { Code: "ja", Name: "日本語" }
    ];

    getLanguages(): ILanguage[] {
        return this.languages;
    }
}




