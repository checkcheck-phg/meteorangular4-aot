import { Injectable, TemplateRef } from "@angular/core";
import { MdDialog, MdDialogRef, MdDialogConfig, ComponentType } from "@angular/material";
import { Observable } from 'rxjs/Rx';

import { BaseMsgDialogComponent } from "./../Components/Modals/BaseMsgDialogComponent"; 
import { ConfirmDialogComponent } from "./../Components/Modals/ConfirmDialogComponent"; 
import { ErrorDialogComponent } from "./../Components/Modals/ErrorDialogComponent"; 
import { WarningDialogComponent } from "./../Components/Modals/WarningDialogComponent"; 
import { InfoDialogComponent } from "./../Components/Modals/InfoDialogComponent"; 
import { ExceptionDialogComponent } from "./../Components/Modals/ExceptionDialogComponent"; 
import { environment } from "./../../Environments/environment";
import { TranslateService } from "./TranslateService";
import { EResource } from "./../Enums/EResource";

@Injectable()
export class ModalService {
    private config: MdDialogConfig = {
        disableClose: true
    };

    constructor(private dialog: MdDialog, private translateService: TranslateService) {}

    confirm(message: string, title?: string): Observable<boolean> {
        return this.showDialogMessage(ConfirmDialogComponent, message, title);
    }

    error(message: string, title?: string): Observable<boolean> {
        return this.showDialogMessage(ErrorDialogComponent, message, title);
    }

    warning(message: string, title?: string): Observable<boolean> {
        return this.showDialogMessage(WarningDialogComponent, message, title);
    }

    info(message: string, title?: string): Observable<boolean> {
        return this.showDialogMessage(InfoDialogComponent, message, title);
    }

    exception(message: string): Observable<boolean> {
        if (!environment.production) {
            return this.showDialogMessage(ExceptionDialogComponent, `<pre>${message}</pre>`);
        }
    }

    notImplemented(): void {
        this.info("Msg_Not_Implemented", "Msg_Infomations");
    }

    show<T>(component: ComponentType<T>): MdDialogRef<T> {
        let config = _.clone(this.config);
        config.width = `${(<any>component).size}px`;
        return this.dialog.open(component, config);
    }

    private showDialogMessage<T extends BaseMsgDialogComponent<T>>(
        component: ComponentType<T>,
        message: string,
        title?: string): Observable<boolean>
    {
        let config = _.clone(this.config);
        config.width = `${(<any>component).size}px`;
        let dialogRef: MdDialogRef<T>;
        dialogRef = this.dialog.open(component, config);
        dialogRef.componentInstance.title = title || dialogRef.componentInstance.title;
        dialogRef.componentInstance.message = message;
        return dialogRef.afterClosed();
    }
}