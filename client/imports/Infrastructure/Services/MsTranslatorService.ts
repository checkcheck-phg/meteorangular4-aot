import { Injectable, Injector } from "@angular/core";
import { Http, RequestOptionsArgs, BaseRequestOptions,
    Headers, RequestOptions, Response, URLSearchParams, Request, RequestMethod, ResponseContentType } from "@angular/http";

import { Utils } from "./../Common/Utils/Utils";
import { TranslateService } from "./TranslateService";
import { CommonConstants } from "../Constants/CommonConstants";
import { ITranslate } from "./../Interfaces/ITranslate";

import * as querystring from "querystring";
import * as http from "http";
import * as https from "https";

type TParams = ITranslate | ITranslate[];

enum ETranslatorTextAPI {
    Translate,
    TranslateArray,
    GetTranslationsArray,
    GetLanguagesForTranslate
}

const URL = {
    Host: "https://api.microsofttranslator.com",
    Translate: "https://api.microsofttranslator.com/V2/Http.svc/Translate",
    SerializationArrays: "http://schemas.microsoft.com/2003/10/Serialization/Arrays",
    TranslateArray: "https://api.microsofttranslator.com/V2/Http.svc/TranslateArray",
    Detect: "https://api.microsofttranslator.com/V2/Http.svc/Detect",
    IssueToken: "https://api.cognitive.microsoft.com/sts/v1.0/issueToken",
}

const REQUEST_BODY = {
    RequestItem: `<string xmlns="http://schemas.microsoft.com/2003/10/Serialization/Arrays">{0}</string>`,
    TranslateArray: `
        <TranslateArrayRequest>
            <AppId>{0}</AppId>
            <From>{1}</From>
            <Options>
                <Category xmlns="http://schemas.datacontract.org/2004/07/Microsoft.MT.Web.Service.V2">general</Category>
                <ContentType xmlns="http://schemas.datacontract.org/2004/07/Microsoft.MT.Web.Service.V2">text/plain</ContentType>
                <ReservedFlags xmlns="http://schemas.datacontract.org/2004/07/Microsoft.MT.Web.Service.V2"/>
                <State xmlns="http://schemas.datacontract.org/2004/07/Microsoft.MT.Web.Service.V2">1</State>
                <Uri xmlns="http://schemas.datacontract.org/2004/07/Microsoft.MT.Web.Service.V2">all</Uri>
                <User xmlns="http://schemas.datacontract.org/2004/07/Microsoft.MT.Web.Service.V2">all</User>
            </Options>
            <Texts>{2}</Texts>
            <To>{3}</To>
        </TranslateArrayRequest>`,
    GetTranslationsArray: `
        <GetTranslationsArrayRequest>
            <AppId>{0}</AppId>
            <From>{1}</From>
            <Options>
                <ContentType xmlns="http://schemas.datacontract.org/2004/07/Microsoft.MT.Web.Service.V2">text/plain</ContentType>
            </Options>
            <Texts>{2}</Texts>
            <To>{3}</To>
            <MaxTranslations>{4}</MaxTranslations>
        </GetTranslationsArrayRequest>`
}

@Injectable()
export class MsTranslatorService {
    private translateService: TranslateService;

    private ERR_PATTERNS = [
        "ArgumentException:",
        "ArgumentOutOfRangeException:",
        "TranslateApiException:",
        "ArgumentNullException:"
    ];

    private tokenApi = {
        host: "api.cognitive.microsoft.com",
        path: "/sts/v1.0/issueToken",
        method: "POST",
        headers: {
            "Ocp-Apim-Subscription-Key": "df9e8c13958141a79b3b56549184a6e9"
        }
    }

    private translateApi = {
        host: "api.microsofttranslator.com",
        path: null,
        method: "GET",
        headers: {
            Authorization: null
        }
    }

    private ajaxRoot = "/V2/Ajax.svc/";
    private httpRoot = "/V2/Http.svc/";
    private autoRefresh = true;
    private appId = "";
    private expiresAt = 0;
    private expiresIn = null;

    constructor(private http: Http) {}

    setTranslateService(service: TranslateService): void {
        this.translateService = service;
    }

    translate(params: ITranslate, fn: Function): void {
        this.makeRequest(ETranslatorTextAPI.Translate, params, fn);
    };

    translateArray(params: TParams, fn: Function): void {
        this.makeRequest(ETranslatorTextAPI.TranslateArray, params, fn, "post");
    };

    getTranslationsArray(params: TParams, fn: Function): void {
        this.makeRequest(ETranslatorTextAPI.GetTranslationsArray, params, fn, "post");
    };

    getLanguagesForTranslate(fn: Function): void {
        this.makeRequest(ETranslatorTextAPI.GetLanguagesForTranslate, null, fn);
    };

    private get(path: ETranslatorTextAPI, param: ITranslate, fn: Function): void {
        let searchParams: URLSearchParams = new URLSearchParams();
        searchParams.set("appid", this.appId);
        if (param) {
            searchParams.set("text", this.printParam(param));
            searchParams.set("from", CommonConstants.DEFAULT_LANGUAGE_SOURCE_TRANSLATE_EXTERNAL);
            searchParams.set("to", this.translateService.language);
        }
        var requestOptions = new RequestOptions({ search: searchParams });
        this.http.get(`${URL.Host}${this.ajaxRoot}${ETranslatorTextAPI[path]}`, requestOptions)
            .subscribe(responce => {
                try {
                    let translation = responce.text().replace(/^\"|\"$/g, "")
                    if (responce.status !== 200) {
                        if (!_.isUndefined(fn)) {
                            fn(new Error(translation), param);
                        }
                        return;
                    }

                    param.Translation = translation;
                    fn(null, param);
                } catch (e) {
                    fn(e, null);
                }
            });
    };

    private post(path: ETranslatorTextAPI, params: TParams, fn: Function): void {
        let body = null;
        if (params) {
            switch(path) {
                case ETranslatorTextAPI.TranslateArray:
                    body = Utils.stringFormat(REQUEST_BODY[ETranslatorTextAPI[path]],
                        this.appId,
                        CommonConstants.DEFAULT_LANGUAGE_SOURCE_TRANSLATE_EXTERNAL,
                        this.getRequestItemStr(params),
                        this.translateService.language);
                    break;
                case ETranslatorTextAPI.GetTranslationsArray:
                    body = Utils.stringFormat(REQUEST_BODY[ETranslatorTextAPI[path]],
                        this.appId,
                        CommonConstants.DEFAULT_LANGUAGE_SOURCE_TRANSLATE_EXTERNAL,
                        this.getRequestItemStr(params),
                        this.translateService.language,
                        (<any>params).length);
                    break;
            }
        }

        // TODO[HCN] API not working
        // this.http.post(`${URL.Host}${this.httpRoot}${ETranslatorTextAPI[path]}`, body)
        //     .subscribe(responce => {
        //         console.log("Test");
        //     });
    }

    private initializeToken(callback: Function, noRefresh: boolean): void {
        var requestOptions = new RequestOptions({
            headers: new Headers({ "Ocp-Apim-Subscription-Key": "df9e8c13958141a79b3b56549184a6e9" })
        });
        this.http.post(URL.IssueToken, "", requestOptions)
            .subscribe(responce => {
                if (responce.status !== 200) {
                    if (!_.isUndefined(callback)) {
                        callback(new Error(`Received: ${responce.text()}
                            when trying to retrieve a new access token.
                            Status code: ${responce.status}`));
                    }
                    return;
                }

                this.appId = `Bearer ${responce.text()}`;
                this.expiresIn = 9 * 60 * 1000; // token is valid for 10 min. So set time before it is expiring
                this.expiresAt = Date.now() + this.expiresIn;

                if (!noRefresh) {
                    setTimeout(this.initializeToken.bind(this), this.expiresIn);
                }
                if (!_.isUndefined(callback)) {
                    callback(null);
                }
            })
    }

    private makeRequest(path: ETranslatorTextAPI, params: TParams, fn: Function, method = "get"): void {
        if (this.autoRefresh && this.expiresAt <= Date.now()) {
            this.initializeToken(() => this[method](path, params, fn), true);
        }
        else {
            this[method](path, params, fn);
        }
    }

    private getRequestItemStr(params: TParams): string {
        return (<string[]>this.printParams(params, text => Utils.stringFormat(REQUEST_BODY.RequestItem, text))).join("");
    }

    private printParam(param: ITranslate, format?: Function): string {
        return format ? format(this.escapeDoubleQuotes(param.Text)) : this.escapeDoubleQuotes(param.Text);
    }

    private printParams(params: TParams, format: Function): string | string[] {
        if (Array.isArray(params)) {
            return params.map(param => this.printParam(param, format));
        }
        return this.printParam(params, format);
    }

    private escapeDoubleQuotes(str: string): string {
        return str.replace(/(^|[^\\])"/g, "$1\\\"");
    }
}