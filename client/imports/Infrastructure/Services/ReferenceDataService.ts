import { Injectable } from "@angular/core";
import { Observable, Subject } from "rxjs";
import { MeteorObservable, ObservableCursor } from "meteor-rxjs";

import { ReferenceData, ReferenceDataValue } from "./../../../../both";
import { BaseModelService } from "./BaseModelService";
import { ModalService } from "./ModalService";
import { EReferenceDataType } from "./../Enums/EReferenceDataType";
import { IReferenceData } from "./../Interfaces/IReferenceData";

@Injectable()
export class ReferenceDataService extends BaseModelService<ReferenceData> {
    private cacheData: IReferenceData = {};
    private referenceDataSubject = new Subject<IReferenceData>();
    private referenceDataResults: Observable<IReferenceData> = this.referenceDataSubject.asObservable();

    constructor(modalService: ModalService) {
        super(ReferenceData.name, modalService);
    }

    getReferenceData(...types: EReferenceDataType[]): Observable<IReferenceData> {
        let unLoadedTypes = types.filter(k => _.isEmpty(this.cacheData[EReferenceDataType[k]]));
        if (unLoadedTypes.length > 0) {
            let typeArr = unLoadedTypes.map(t => EReferenceDataType[t]);
            this.call<ReferenceData[]>("FindByType", typeArr, responce => {
                responce.forEach(r => this.cacheData[r.Type] = r.ReferenceDataValues);
                this.handleSubscribeResult(types);
            })
        } else {
            this.handleSubscribeResult(types);
        }
        return this.referenceDataResults;
    }

    loadReferenceData(types: EReferenceDataType[], callback: Function): void {
        let unLoadedTypes = types.filter(k => _.isEmpty(this.cacheData[EReferenceDataType[k]]));
        if (unLoadedTypes.length > 0) {
            let typeArr = unLoadedTypes.map(t => EReferenceDataType[t]);
            this.call<ReferenceData[]>("FindByType", typeArr, responce => {
                responce.forEach(r => this.cacheData[r.Type] = r.ReferenceDataValues);
                this.handleSubscribeResult(types, callback);
            })
        } else {
            this.handleSubscribeResult(types, callback);
        }
    }

    private handleSubscribeResult(types: EReferenceDataType[], callback?: Function) {
        let typeArr = types.map(t => EReferenceDataType[t]);
        let results = Object.keys(this.cacheData).reduce((data, k) => {
            if (_.contains(typeArr, k)) {
                data[k] = this.cacheData[k]
            }
            return data;
        }, {});

        if (callback) {
            callback(results);
        } else {
            this.referenceDataSubject.next(results);
        }
    }
}