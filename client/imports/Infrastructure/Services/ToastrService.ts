
import { Injectable } from "@angular/core";

import { TranslateService } from "../Services/TranslateService";
import { EToastrType } from "../Enums/EToastrType";
import { EResource } from "../Enums/EResource";

import * as toastr from "toastr";

@Injectable()
export class ToastrService {
    private defaultOptions: ToastrOptions = {
        closeButton: false,
        debug: false,
        newestOnTop: true,
        progressBar: true,
        positionClass: "toast-bottom-right",
        preventDuplicates: true,
        showDuration: 300,
        hideDuration: 1000,
        timeOut: 2000,
        extendedTimeOut: 1000,
        showEasing: "swing",
        hideEasing: "linear",
        showMethod: "fadeIn",
        hideMethod: "fadeOut"
    }

    constructor(private translateService: TranslateService) {

    }

    error(message: string, title?: string): void {
        this.show(EToastrType.ERROR, message, title, {
            // Override not implemented
        });
    }

    info(message: string, title?: string): void {
        this.show(EToastrType.INFO, message, title, {
            // Override not implemented
        });
    }

    success(message: string, title?: string): void {
        this.show(EToastrType.SUCCESS, message, title, {
            // Override not implemented
        });
    }

    warning(message: string, title?: string): void {
        this.show(EToastrType.WARNING, message, title, {
            // Override not implemented
        });
    }

    notImplemented(): void {
        this.info("Msg_Not_Implemented", "Info_Title");
    }

    show(type: EToastrType, message: string, title: string, overrideOptions: ToastrOptions): void {
        this.translateService.translate([message, title], EResource.Message)
            .subscribe(translation => toastr[EToastrType[type].toLowerCase()](
                translation[message],
                translation[title],
                _.extend({}, this.defaultOptions, overrideOptions)
            ));
    }
}