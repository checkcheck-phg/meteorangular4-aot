import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { Http } from "@angular/http";

import { Observable, Subject } from "rxjs";

import { LanguageService } from "./LanguageService";
import { LogService } from "../../../../both";
import { MsTranslatorService } from "./MsTranslatorService";
import { CommonConstants } from "./../Constants/CommonConstants";
import { EResource } from "./../Enums/EResource";
import { ITranslate } from "./../Interfaces/ITranslate";
import { Utils } from "./../Common/Utils/Utils";

const PREFIX = "./resources";
const SUFFIX = ".json";
@Injectable()
export class TranslateService {
    private currentLanguage = CommonConstants.DEFAULT_LANGUAGE;
    private resources: Object = {};
    private pendingUrls = {};
    private onLanguageChanged = new Subject<string>();
    languageChanged = this.onLanguageChanged.asObservable().share();

    constructor(private http: Http,
        private router: Router,
        private languageService: LanguageService,
        private logService: LogService,
        private msTranslatorService: MsTranslatorService
    ) {
        msTranslatorService.setTranslateService(this);
        this.initial();
    }

    translate(key: string | Array<string>, resource?: EResource): Observable<Object> {
        let resourcePath = this.getResourcePath(resource);
        let keyArr = _.isArray(key) ? key : [key];
        return this.getResourceText(_.compact(keyArr), resourcePath);
    }

    private initial(): void {
        this.languageService.getLanguages().forEach(language => this.resources[language.Code.toLowerCase()] = {});
    }

    private getResourceText(key: string | Array<string>, resourcePath: string): Observable<Object> {
        let onResourceReady = new Subject<any>();
        let resourceReady = onResourceReady.asObservable();
        this.getResourceTextHandler(onResourceReady, key, resourcePath);
        return resourceReady;
    }

    private getResourceTextHandler(onResourceReady: Subject<any>, key: string | Array<string>, resourcePath: string): void {
        var keyArr = _.isArray(key) ? key : [key];
        let resource;
        try {
            resource = this.getDeepValue(this.resources[this.currentLanguage], resourcePath);
            if (!resource) {
                let url = `${PREFIX}/${resourcePath}/${this.currentLanguage}${SUFFIX}`;
                if (!this.pendingUrls[url]) {
                    this.pendingUrls[url] = this.loadResource(url);
                }
                this.pendingUrls[url]
                    .subscribe(result => {
                        this.setDeepValue(this.resources[this.currentLanguage], result, resourcePath);
                        this.resourceReadyHandler(onResourceReady, keyArr, result, resourcePath);
                        this.pendingUrls[url] = null;
                    });
                return;
            }
        } catch(err) {
            // TODO[HCN] Do not support for translate reource of children router yet
            this.logService.warn("Not implemented !!!");
        }

        this.resourceReadyHandler(onResourceReady, keyArr, resource, resourcePath);
    }

    private loadResource(url: string): Observable<any> {
        return this.http.get(url).map(res => {
            try {
                return res.json();
            } catch(err) {
                return {};
            }
        }).share();
    }

    private getResourcePath(resource?: EResource): string {
        if (!_.isNumber(resource)) {
            let resourcePath = this.router.url.replace(/^\//, "");
            return (_.isEmpty(resourcePath) ? EResource[EResource.Home] : resourcePath).toLowerCase();
        }
        return EResource[resource].toLowerCase();
    }

    private getDeepValue(resource: Object, resourcePath: string|string[]): any {
        let resourcePathArr = _.isArray(resourcePath) ? resourcePath : resourcePath.split(/\/|\./g);
        let value = resource[resourcePathArr.shift()];
        if (value && resourcePathArr.length > 0) {
            return this.getDeepValue(value, resourcePathArr);
        }
        return value;
    }

    private setDeepValue(resource: Object, value: any, resourcePath: string|string[]): void {
        let resourcePathArr = _.isArray(resourcePath) ? resourcePath : resourcePath.split(/\/|\./g);
        let property = resourcePathArr.shift();

        if (resourcePathArr.length === 0) {
            resource[property] = _.extend(resource[property] || {}, value);
            return;
        }

        if (!resource[property]) {
            resource[property] = { _unknow: true };
        }

        this.setDeepValue(resource[property], value, resourcePathArr);
    }

    private resourceReadyHandler(onResourceReady: Subject<any>, keyArr: string[], resource: Object, resourcePath: string): void {
        let resourceText = {};
        let missingkeys = {};
        keyArr.forEach(k => {
            var text = this.getDeepValue(resource, k);
            if (text) {
                resourceText[k] = text;
            } else {
                missingkeys[k] = text;
            }
        });
        if (_.isEmpty(missingkeys)) {
            setTimeout(() => onResourceReady.next(resourceText));
        } else {
            this.resourceTextMissingHandler(onResourceReady, resourceText, missingkeys, resourcePath);
        }
    }

    private resourceTextMissingHandler(onResourceReady: Subject<any>, resourceText: Object, missingkeys: Object, resourcePath: string): void {
        let resourceEnglish = this.getDeepValue(this.resources["en"], resourcePath);
        if (!resourceEnglish) {
            let url = `${PREFIX}/${resourcePath}/en${SUFFIX}`;
            if (!this.pendingUrls[url]) {
                this.pendingUrls[url] = this.loadResource(url);
            }
            this.pendingUrls[url]
                .subscribe(result => {
                    this.setDeepValue(this.resources["en"], result, resourcePath);
                    this.translateFromThirdParty(onResourceReady,
                        this.getTranslateParams(missingkeys, result),
                        resourceText,
                        resourcePath);
                    this.pendingUrls[url] = null;
                });
            return;
        }

        this.translateFromThirdParty(onResourceReady,
            this.getTranslateParams(missingkeys, resourceEnglish),
            resourceText,
            resourcePath);
    }

    private getTranslateParams(missingkeys: Object, resourceEnglish: Object): ITranslate[] {
        let params = [];
        Object.keys(missingkeys).forEach(k => params.push({ Key: k, Text: this.getDeepValue(resourceEnglish, k) }));
        return params;
    }

    private translateFromThirdParty(onResourceReady: Subject<any>,
        translateParams: ITranslate[],
        resourceText: Object,
        resourcePath: string): void
    {
        translateParams.forEach(param => {
            resourceText[param.Key] = `${resourcePath}.${this.language}:${param.Key}`;
        });
        onResourceReady.next(resourceText);
    }

    get language(): string {
        return this.currentLanguage;
    }

    set language(language: string) {
        this.currentLanguage = language;
        this.onLanguageChanged.next(this.currentLanguage);
    }
}
