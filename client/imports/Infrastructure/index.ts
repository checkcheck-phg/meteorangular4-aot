// IMPORTS
import { GoogleMapsAPIWrapper } from "angular2-google-maps/core";

import { LogService, ConfigService } from "../../../both";
import { CommonService } from "./Services/CommonService";
import { ModalService } from "./Services/ModalService";
import { TranslateService } from "./Services/TranslateService";
import { CountryService } from "./Services/CountryService";
import { LanguageService } from "./Services/LanguageService";
import { ToastrService } from "./Services/ToastrService";
import { MsTranslatorService } from "./Services/MsTranslatorService";
import { ReferenceDataService } from "./Services/ReferenceDataService";

import { Authentication } from "./Models/Authentication";

// EXPORTS
export const INFRASTRUCTURE_INJECTABLES = [
    ConfigService,
    LogService,

    CommonService,
    ModalService,
    TranslateService,
    CountryService,
    LanguageService,
    ToastrService,
    MsTranslatorService,
    GoogleMapsAPIWrapper,
    ReferenceDataService,
    Authentication
];

export * from "../../../both";

// Modules
export * from "./InfrastructureModule";

// Annotations
export * from "./Annotations/ModalComponent";

// Annotations
export * from "./Constants/CommonConstants";

// Abstract class
export * from "./Components/BaseComponent";
export * from "./Components/BaseContentComponent";
export * from "./Components/Modals/BaseModalComponent";
export * from "./Services/BaseModelService";
export * from "./Models/BaseModel";

// Models
export * from "./Models/Authentication";

// Enums
export * from "./Enums/EModalSize";
export * from "./Enums/EResource";
export * from "./Enums/EViewMode";
export * from "./Enums/EToastrType";
export * from "./Enums/EReferenceDataType";

// Interfaces
export * from "./Interfaces/IModalButtonFooter";
export * from "./Interfaces/IMarker";
export * from "./Interfaces/IReferenceData";

// Services
export * from "./Services/CommonService";
export * from "./Services/ExceptionHandler";
export * from "./Services/ModalService";
export * from "./Services/TranslateService";
export * from "./Services/ToastrService";
export * from "./Services/MsTranslatorService";
export * from "./Services/ReferenceDataService";

// Utils
export * from "./Common/Utils/Utils";
export * from "./Common/Utils/DateUtils";