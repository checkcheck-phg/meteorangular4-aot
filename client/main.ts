import "angular2-meteor-polyfills";

import { enableProdMode,  } from "@angular/core";
import { platformBrowserDynamic } from "@angular/platform-browser-dynamic";
import { Meteor } from "meteor/meteor";

import { AppModule } from "./imports/App/AppModule";
import { environment } from "./imports/Environments/environment";

Meteor.startup(() => platformBrowserDynamic().bootstrapModule(AppModule));

enableProdMode();