import { Injectable } from "@angular/core";
import { Decorator } from "./../../../both/deprecated/Decorator";

export const Controller = Decorator.makeDecorator("Controller", [], Injectable)