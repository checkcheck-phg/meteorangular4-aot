import { Meteor } from "meteor/meteor";
import { injector } from "../publications";

export const Method = () => {
    return function (target, propertyKey: string, descriptor: PropertyDescriptor) {
        let method = {}
        method[`${target.constructor.name.replace("Controller", "")}_${propertyKey}`] = ((...params) => {
            let instance = injector.get(target.constructor);

            console.log(`METHOD CALL: ${target.constructor.name}.${propertyKey}`);
            return instance[propertyKey](...params);
        });
        Meteor.methods(method);
    }
}