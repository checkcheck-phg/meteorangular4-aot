import { Meteor } from "meteor/meteor";
import { injector } from "../publications";

export const Publish = () => {
    return function (target, propertyKey: string, descriptor: PropertyDescriptor) {
        Meteor.publish(`${target.constructor.name.replace("Controller", "")}_${propertyKey}`, (...params) => {
            let instance = injector.get(target.constructor);

            console.log(`PUBLISH CALL: ${target.constructor.name}.${propertyKey}`);
            return instance[propertyKey](...params);
        });
    }
}