import { Meteor } from "meteor/meteor";
import { injector } from "../publications";

interface IRuleParams {
    insert?: boolean;
    remove?: boolean;
    update?: boolean;
}

declare var collection;

export const Rules = (params: IRuleParams) => {
    return function (target, propertyKey: string, descriptor: PropertyDescriptor) {
        const originalMethod = descriptor.value;
        var rules = Object.keys(params).reduce((prev, r) => {
            prev[r] = (doc) => Boolean(params[r]);
            return prev;
        }, {});
        descriptor.value = (...args: any[]) => {
            let instance = injector.get(target.constructor);
            if (!_.isEmpty(rules)) {
                instance.collection.allow(rules);
            }
            return originalMethod.apply(instance, args);
        };

        return descriptor;
    }
}