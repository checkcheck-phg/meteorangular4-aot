import { Injectable } from "@angular/core";
import { Decorator } from "./../../../both/deprecated/Decorator";

export const Service = Decorator.makeDecorator("Service", [], Injectable)