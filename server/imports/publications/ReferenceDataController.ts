import { BaseController } from "./BaseController";
import { Controller, Method } from "./../annotations";
import { ReferenceDataRepository } from "./../repositories";
import { ReferenceData } from "./../../../both";

@Controller()
export class ReferenceDataController extends BaseController {
    constructor(private referenceDataRepository: ReferenceDataRepository) {
        super();
    }

    @Method()
    FindByType(typeArr: string[]): ReferenceData[] {
        return this.referenceDataRepository.findByType(typeArr);
    }
}