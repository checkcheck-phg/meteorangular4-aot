import "reflect-metadata";
import { ReflectiveInjector } from "@angular/core";

import { SERVICE_PROVIDERS, ConfigService } from "../services";
import { REPOSITORIE_PROVIDERS } from "../repositories";
import { ReferenceDataController } from "./ReferenceDataController";

export const CONTROLLER_PROVIDERS = [
    ReferenceDataController
];

export const injector = ReflectiveInjector.resolveAndCreate([
    ...SERVICE_PROVIDERS,
    ...REPOSITORIE_PROVIDERS,
    ...CONTROLLER_PROVIDERS
]);
injector.get(ConfigService);