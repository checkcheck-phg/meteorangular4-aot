import { MongoObservable } from "meteor-rxjs";

export abstract class BaseRepository<T> {
    constructor(public collection: MongoObservable.Collection<T>) {
        console.log(`REPOSITORY INITIALED: ${this.constructor.name}`);
    }
}