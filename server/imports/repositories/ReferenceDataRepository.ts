import { Repository } from "../annotations";
import { BaseRepository } from "./BaseRepository";
import { ReferenceData, ReferenceDataCollection } from "./../../../both";

@Repository()
export class ReferenceDataRepository extends BaseRepository<ReferenceData> {
    constructor() {
        super(ReferenceDataCollection);
    }

    findByType(typeArr: string[]): ReferenceData[] {
        return this.collection.find({ Type: {$in: typeArr} }).fetch();
    }
}