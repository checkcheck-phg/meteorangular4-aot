import { ReferenceDataRepository } from "./ReferenceDataRepository";

export const REPOSITORIE_PROVIDERS = [
    ReferenceDataRepository
];

export * from "./BaseRepository";
export * from "./ReferenceDataRepository";
