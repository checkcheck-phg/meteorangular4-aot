import { LogService, ConfigService, ISettings  } from "../../../both";

export abstract class BaseService {
    constructor(protected logService: LogService, protected configService: ConfigService) {
        console.log(`SERVICE INITIALED: ${this.constructor.name}`);
        this.startup();
    }

    protected startup(): void {
        // Override from child class
    }

    get settings(): ISettings {
        return <ISettings>this.configService.settings;
    }
}