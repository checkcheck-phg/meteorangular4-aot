import { LogService, ConfigService } from "../../../both";

export const SERVICE_PROVIDERS = [
    LogService,
    ConfigService
];

export * from "../../../both/services";
