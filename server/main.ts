﻿import { Meteor } from "meteor/meteor";
import { Accounts } from "meteor/accounts-base";

import "./imports/publications";

Meteor.startup(() => {
    // code to run on server at startup
});